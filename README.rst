===========
Annalis API
===========

Api for create sites web with blog, pages, galleries and taxonomies.

Quick start
-----------

1. Add "myblog" to INSTALLED_APPS:
  INSTALLED_APPS = {
    ...
    'oauth'
  }

2. Include the myblog URLconf in urls.py:
  url(r'^api, include('api.urls')),

3. Run `python manage.py migrate` to create annalise models.

4. Run the development server and access http://127.0.0.1:8000/admin

5. Access http://127.0.0.1:8000/api/site/{id_site}/posts to view a list of most recent posts.