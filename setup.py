import os
from setuptools import setup

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

# Allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name='ts-oauth',
    version='1.0',
    packages=['oauth'],
    include_package_data=True,
    install_requires=required,
    license='BSD License',
    description='Api for create sites web',
    long_description=README,
    url='https://www.tingsystems.com/',
    author='Tingsystems',
    author_email='soporte@tingsystems.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.4.1',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content'
    ]
)
