__author__ = 'tingsystems'

import logging
import json
from django.http import HttpResponse
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from oauth2_provider.views import TokenView, RevokeTokenView
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope
from oauth2_provider.models import AccessToken
from rest_framework import permissions
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, GenericAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import status
from rest_framework import filters

from .models import User, Group, Permission, Site
from .serializers import (UserSerializer, GroupSerializer, GroupsToUserSerializer, PermissionSerializer, SiteSerializer)
from .permissions import OAuthObjectPermissions
from oauth import utils

logger = logging.getLogger(__name__)


class LoginView(TokenView):
    @method_decorator(sensitive_post_parameters('password'))
    def post(self, request, *args, **kwargs):
        url, headers, body, status = self.create_token_response(request)
        if status == 200:
            body_json = json.loads(body)
            token = AccessToken.objects.get(token=body_json['access_token'])
            user = token.user
            body_json['author'] = {
                'firstName': user.firstName, 'lastName': user.lastName, 'email': user.email,
                'permissions': utils.get_user_permissions(user), 'is_superuser': user.is_superuser
            }
            body_json['sites'] = [SiteSerializer(site).data for site in token.user.sites.all()]
            body = json.dumps(body_json)
        response = HttpResponse(content=body, status=status)
        for k, v in headers.items():
            response[k] = v
        logger.info(_('Token of  authentication generated ok for user'.format(request.user)))
        return response


class LogoutView(RevokeTokenView):
    def post(self, request, *args, **kwargs):
        url, headers, body, status = self.create_revocation_response(request)
        response = HttpResponse(content=body or '', status=status)
        for k, v in headers.items():
            response[k] = v
        return response


class ListCreateMixin(ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope, OAuthObjectPermissions]
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, filters.DjangoFilterBackend)


class RetrieveUpdateMixin(RetrieveUpdateAPIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope, OAuthObjectPermissions]


class SiteListCreateView(ListCreateMixin):
    serializer_class = SiteSerializer

    def get_queryset(self):
        return Site.objects.all()


class SiteRetrieveUpdateView(RetrieveUpdateMixin):
    serializer_class = SiteSerializer
    lookup_field = 'pk'

    def get_object(self):
        return get_object_or_404(Site, id=self.kwargs['pk'])


class UserListCreateView(ListCreateMixin):
    filter_fields = ('firstName', 'username', 'email', 'is_active', 'date_joined')
    ordering_fields = ('firstName', 'username', 'email', 'is_active', 'date_joined')
    search_fields = ('firstName', 'username', 'email', 'is_active', 'date_joined')
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.all()


class UserRetrieveUpdateView(RetrieveUpdateMixin):
    serializer_class = UserSerializer
    lookup_field = 'pk'
    queryset = User.objects.all()

    def get_object(self):
        return get_object_or_404(User, id=self.kwargs['pk'])


class GroupListCreateView(ListCreateMixin):
    filter_fields = ('name', 'createdAt', 'isActive')
    ordering_fields = ('name', 'createdAt', 'isActive')
    search_fields = ('name', 'createdAt', 'isActive')
    serializer_class = GroupSerializer

    def get_queryset(self):
        return Group.objects.all()


class GroupRetrieveUpdateView(RetrieveUpdateMixin):
    serializer_class = GroupSerializer
    lookup_field = 'pk'
    queryset = Group.objects.all()

    def get_object(self):
        return get_object_or_404(Group, id=self.kwargs['pk'])


class GroupsToUserView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]
    serializer_class = GroupsToUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.create(request.data), status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.delete(request.data), status=status.HTTP_201_CREATED)


class PermissionsListCreateView(ListCreateMixin):
    serializer_class = PermissionSerializer

    def get_queryset(self):
        return Permission.objects.all()
