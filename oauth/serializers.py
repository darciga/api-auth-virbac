__author__ = 'tingsystems'

import logging
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.generics import get_object_or_404
from rest_framework.exceptions import ValidationError
from .models import Site, User, Group, Permission
from oauth import utils

logger = logging.getLogger(__name__)


class SiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    permissions = serializers.SerializerMethodField()
    sites = serializers.PrimaryKeyRelatedField(many=True, queryset=Site.objects.all())
    groups = serializers.PrimaryKeyRelatedField(many=True, queryset=Group.objects.all())

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'firstName', 'lastName', 'groups', 'permissions', 'user_permissions',
                  'date_joined', 'is_active', 'password', 'sites', 'is_superuser']
        extra_kwargs = {
            'password': {'write_only': True, 'required': False},
            'user_permissions': {'write_only': True, 'required': False},
        }

    def create(self, validated_data):
        if not validated_data.get('password'):
            raise ValidationError(_('The password not empty'))
        instance = super(UserSerializer, self).create(validated_data)
        instance.set_password(validated_data.get('password'))
        instance.save()
        logger.info(_('User {} created ok'.format(instance.username)))
        return instance

    def update(self, instance, validated_data):
        instance = super(UserSerializer, self).update(instance=instance, validated_data=validated_data)
        if validated_data.get('password'):
            instance.set_password(validated_data.get('password'))
            instance.save()
        logger.info(_('User {} update ok'.format(instance.username)))
        return instance

    @classmethod
    def get_permissions(cls, obj):
        groups = obj.groups.all()
        response_permissions = []
        # add permissions from the groups
        for group in groups:
            permissions = group.permissions.all()
            for permission in permissions:
                if utils.permission_exist(permission.id, response_permissions):
                    continue
                response_permissions.append(PermissionSerializer(permission).data)
        # add permissions individuals
        for permission in obj.user_permissions.all():
            if not utils.permission_exist(permission.id, response_permissions):
                continue
            response_permissions.append(PermissionSerializer(permission).data)
        return response_permissions or None


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name', 'isActive', 'createdAt', 'updatedAt', 'permissions']


class GroupsToUserSerializer(serializers.Serializer):
    groups = serializers.ListField(child=serializers.IntegerField())
    user = serializers.UUIDField()

    @classmethod
    def create(cls, validated_data):
        groups_ids = validated_data.get('groups', [])
        user = get_object_or_404(User, id=validated_data.get('user'))
        for id_group in groups_ids:
            group = Group.objects.filter(id=id_group).first()
            if group:
                user.groups.add(group)
        logger.info(_('Groups add to user {} ok'.format(user.username)))
        return user.groups.all().values()

    @classmethod
    def delete(cls, validated_data):
        groups_ids = validated_data.get('groups', [])
        user = get_object_or_404(User, id=validated_data.get('user'))
        for id_group in groups_ids:
            group = Group.objects.filter(id=id_group).first()
            if group:
                user.groups.remove(group)
        logger.info(_('Groups delete to user {} ok'.format(user.username)))
        return user.groups.all().values()


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = '__all__'
